<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['home'])){
		include __DIR__.'/../pages/home.php';
	}else if(!isset($_GET['contact'])){
		include __DIR__.'/../pages/contact.php';
	} else if(!isset($_GET['bio'])){
		include __DIR__.'/../pages/bio.php';
	}else {
		echo "Error 404";
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData(){

$file = file_get_contents('../data/user.json');
	return json_decode($file);
}